import { Injectable } from '@angular/core';
import { Http ,Response } from '@angular/http';
import { RecipieService } from '../recipies/recipie.service'
import { Recipie  } from '../recipies/recipie.model';
import { AuthService } from '../auth/auth.service';
import 'rxjs/Rx';

@Injectable()
export class DataStorageService {

  constructor(
    private http: Http ,
    private recipieService: RecipieService,
    private authService: AuthService,
  ) { }

  storeRecipie(){
    const token = this.authService.getToken();
    return this.http.put('https://angular4recipeiapp.firebaseio.com/recipeis.json?auth='+token , this.recipieService.getRecipies());
  }

  getRecipies(){
    const token = this.authService.getToken();
    this.http.get('https://angular4recipeiapp.firebaseio.com/recipeis.json?auth='+token).map(
      (response: Response) => {
        const recipies: Recipie[] = response.json();
        for(let recipie of recipies){
          if(!recipie['ingredients']){
            console.log(recipie);
            recipie['ingredients'] = [];
          }
        }
        return recipies;
      }
    ).subscribe(
      (recipies: Recipie[]) => {
        this.recipieService.setRecipies(recipies);
      }
    );
  }

}
