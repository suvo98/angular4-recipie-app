import { Component, OnInit , OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Router , ActivatedRoute } from '@angular/router';
import { Recipie } from '../recipie.model';
import { RecipieService } from '../recipie.service';

@Component({
  selector: 'app-recipies-list',
  templateUrl: './recipies-list.component.html',
  styleUrls: ['./recipies-list.component.css']
})
export class RecipiesListComponent implements OnInit  , OnDestroy {
  recipies: Recipie[];
  subscription: Subscription;


  constructor(private recipieService: RecipieService,
private router: Router,
private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.subscription = this.recipieService.recipieChanged.subscribe(
      (recipies: Recipie[]) => {
        this.recipies = recipies;
      }
    );
    this.recipies = this.recipieService.getRecipies();
  }

onNewRecipie(){
  this.router.navigate(['new'], {relativeTo: this.route});
}

ngOnDestroy(){
  this.subscription.unsubscribe();
}

}
