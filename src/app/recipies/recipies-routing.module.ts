import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes , RouterModule } from '@angular/router';

import { RecipiesComponent } from './recipies.component';
import { RecipiesListComponent } from './recipies-list/recipies-list.component';
import { RecipiesDetailsComponent } from './recipies-details/recipies-details.component';
import { RecipiesItemComponent } from './recipies-list/recipies-item/recipies-item.component';
import { RecipiesStartComponent } from './recipies-start/recipies-start.component';
import { RecipieEditComponent } from './recipie-edit/recipie-edit.component';

import { AuthGuardService } from '../auth/auth-guard.service';

const recipiesRoutes: Routes = [

  {path: '' , component: RecipiesComponent , children: [
    {path: '' , component: RecipiesStartComponent},
    {path: 'new' , component: RecipieEditComponent , canActivate: [AuthGuardService]},
    {path: ':id' , component: RecipiesDetailsComponent},
    {path: ':id/edit' , component: RecipieEditComponent , canActivate: [AuthGuardService]},
  ]},


];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(recipiesRoutes),
  ],
  exports: [RouterModule],
  declarations: []
})
export class RecipiesRoutingModule { }
