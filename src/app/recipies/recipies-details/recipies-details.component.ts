import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Params , Router } from '@angular/router';
import { Recipie } from '../recipie.model';
import { RecipieService } from '../recipie.service';
@Component({
  selector: 'app-recipies-details',
  templateUrl: './recipies-details.component.html',
  styleUrls: ['./recipies-details.component.css']
})
export class RecipiesDetailsComponent implements OnInit {
  recipie: Recipie;
  id: number;
  constructor(private recipieService: RecipieService,
     private route: ActivatedRoute,
     private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.recipie = this.recipieService.getRecipie(this.id);
      }
    );
  }

  onAddToShopList(){
    this.recipieService.addIngredientsToShopList(this.recipie.ingredients);
  }

  onEditRecipie(){
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDeleteRecipie(){
    this.recipieService.deleteRecipie(this.id);
    this.router.navigate(['/recipies']);
  }

}
