import { NgModule } from '@angular/core';
import { FormsModule , ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { RecipiesComponent } from './recipies.component';
import { RecipiesListComponent } from './recipies-list/recipies-list.component';
import { RecipiesDetailsComponent } from './recipies-details/recipies-details.component';
import { RecipiesItemComponent } from './recipies-list/recipies-item/recipies-item.component';
import { RecipiesStartComponent } from './recipies-start/recipies-start.component';
import { RecipieEditComponent } from './recipie-edit/recipie-edit.component';

import { RecipiesRoutingModule } from './recipies-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    RecipiesRoutingModule,
    SharedModule
  ],
  declarations: [
    RecipiesComponent,
    RecipiesListComponent,
    RecipiesDetailsComponent,
    RecipiesItemComponent,
    RecipiesStartComponent,
    RecipieEditComponent,
  ]
})
export class RecipiesModule { }
