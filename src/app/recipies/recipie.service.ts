import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Recipie } from './recipie.model';
@Injectable()

export class RecipieService {
  recipieChanged = new Subject<Recipie[]>();

  private recipies: Recipie[] = [
    new Recipie('A test recipie  1',
    'This is sample text of recipie 1', 'http://media2.sailusfood.com/wp-content/uploads/2013/08/crispy-onion-samosa-recipe.jpg',
    [
      new Ingredient('Meat',1),
      new Ingredient('Fish',23)
    ]
  ),
    new Recipie('A test recipie 2',
    'This is sample text of recipie 2',
     'https://imagesvc.timeincapp.com/v3/mm/image?url=http%3A%2F%2Fcdn-image.myrecipes.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2F4_3_horizontal_-_1200x900%2Fpublic%2Fimage%2Frecipes%2Fck%2F08%2F10%2Fgrilled-chicken-ck-1842358-x.jpg%3Fitok%3D9bXU-_nl&w=700&q=85',
     [
       new Ingredient('Buns',5),
       new Ingredient('Chiken',2)
     ]
   ),
  ];

  constructor(private shoppingListService: ShoppingListService) { }

  getRecipies(){
    return this.recipies.slice();
  }

  setRecipies(recipies: Recipie[]){
    this.recipies = recipies;
    this.recipieChanged.next(this.recipies.slice());
  }

  getRecipie(index: number){
    return this.recipies[index];
  }

  addIngredientsToShopList(ingredients: Ingredient[]){
    this.shoppingListService.addIngredients(ingredients);
  }

  addRecipie(recipie: Recipie){
    this.recipies.push(recipie);
    this.recipieChanged.next(this.recipies.slice())
  }

  updateRecipie(index: number , newRecipie: Recipie){
    this.recipies[index] = newRecipie;
    this.recipieChanged.next(this.recipies.slice());
  }

  deleteRecipie(index: number){
    this.recipies.splice(index,1);
    this.recipieChanged.next(this.recipies.slice());
  }

}
