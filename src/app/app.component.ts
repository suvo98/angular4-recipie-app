import { Component , OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  loadedFeature = 'recipie';
  ngOnInit(){
    firebase.initializeApp({
      apiKey: "AIzaSyDlv5502IwszD4OTSEurann3TGBJSS9Di4",
      authDomain: "angular4recipeiapp.firebaseapp.com",
    });
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
